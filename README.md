# CI/CD for Mikrotik in AWS via terraform.

Following CI variables should be present:
> For simplicity, you can use an AWS user with group `AmazonEC2FullAccess`.
- `TF_VAR_region` - Target AWS region.
- `TF_VAR_access_key` - AWS user access key.
- `TF_VAR_secret_key`- AWS user secret key.
- `GITLAB_TF_PASSWORD` - GitLab [Personal Access Token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html#create-a-personal-access-token) with the api scope. To store [terraform state](https://docs.gitlab.com/ee/user/infrastructure/terraform_state.html).
