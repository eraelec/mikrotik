resource "aws_security_group" "mikrotik" {
  egress {
    from_port = 0
    protocol = "-1"
    to_port = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port = 0
    protocol = "-1"
    to_port = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Name: "mikrotik"
  }
}

resource "aws_instance" "mikrotik" {
  ami = "ami-04862d752f6235302"
  instance_type = "t2.nano"
  security_groups = [
    aws_security_group.mikrotik.name
  ]
  tags = {
    Name = "mikrotik"
  }
}

resource "aws_eip" "mikrotik" {
  vpc = true
  instance                  = aws_instance.mikrotik.id
  tags = {
    Name = "mikrotik"
  }
}
